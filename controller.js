var myApp = angular.module('myApp', []);

myApp.controller('controller', ['$scope', function($scope) {

  $scope.playlist1 = [
    {
      title: 'I wanna be yours',
      artist: 'Arctic Monkeys',
      cover: 'gallery/monkeys.jpg',
    },
    {
      title: 'I\'ll never be your Maggie May',
      artist: 'Suzanne Vega',
      cover: 'gallery/vega.jpg',
    },
    {
      title: 'Where is my mind',
      artist: 'The Pixies',
      cover: 'gallery/pixies.jpg',
    },
    {
      title: 'Tallulah',
      artist: 'FAUVE',
      cover: 'gallery/fauve.jpeg',
    },
    {
      title: 'Lula',
      artist: 'Saez',
      cover: 'gallery/saez.jpg',
    },
    {
      title: 'November',
      artist: 'Brennan Savage',
      cover: 'gallery/savage.jpg',
    },
  ];

  $scope.playlist2 = [
    {
      title: 'Seven nation army',
      artist: 'White Stripes',
      cover: 'gallery/monkeys.jpg',
    },
    {
      title: 'Drunk',
      artist: 'Ed Sheeran',
      cover: 'gallery/vega.jpg',
    },
    {
      title: 'Follow you',
      artist: 'Bring me the Horizon',
      cover: 'gallery/pixies.jpg',
    },
    {
      title: 'Hurricane',
      artist: '30 seconds to Mars',
      cover: 'gallery/fauve.jpeg',
    },
    {
      title: 'Kick me',
      artist: 'Sleeping with Sirens',
      cover: 'gallery/saez.jpg',
    },
    {
      title: 'Somewhere in Neverland',
      artist: 'All Time Low',
      cover: 'gallery/savage.jpg',
    },
  ];

  // ---------------------------



}]);
